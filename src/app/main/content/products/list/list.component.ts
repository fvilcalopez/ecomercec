import { Component, OnInit } from '@angular/core';
import {ProductsService} from "../../../../core/api/products.service";
import {IProduct} from "../../../../core/Model";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  listProducts: IProduct[] = []
  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {
    this.productsService.getAllProducts().subscribe(
      data => {
        this.listProducts = data;
          console.log(data);
      }
    );
  }

  selectArea(e: string) {
    this.listProducts = [];
    this.productsService.getProductsByCategory(e).subscribe(
      data => {
        this.listProducts = data;
      }
    )
    console.log(e);
  }

}
