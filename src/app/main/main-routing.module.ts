import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
  path: '',
  component: MainComponent,
    children:  [
      {path: 'cart', loadChildren: () => import('./content/cart/cart.module').then(m => m.CartModule)},
      {path: 'products', loadChildren: () => import('./content/products/products.module').then(m => m.ProductsModule)},
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
