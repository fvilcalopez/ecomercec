import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MediaMatcher } from '@angular/cdk/layout';
import { SideNavService } from '../core/api/side-nav.service';
import {ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  @ViewChild('sidenav') public sidenav!: MatSidenav;
  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              private sidenavService: SideNavService,
              private router: Router,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    const thos = this;
    setTimeout(function() {
      thos.sidenavService.sideNavToggleSubject.subscribe(()=> {
        thos.sidenav.toggle().then(r => {});
      });
    },200);
    setTimeout(function() {
      thos.sidenav.toggle().then(r => {});
    },200);
  }

}
