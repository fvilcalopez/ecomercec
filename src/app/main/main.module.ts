import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import {LayoutsModule} from "../shared/layouts/layouts.module";
import {MatSidenavModule} from "@angular/material/sidenav";
import {ContentModule} from "./content/content.module";


@NgModule({
  declarations: [
    MainComponent
  ],
    imports: [
        CommonModule,
        MainRoutingModule,
        LayoutsModule,
        MatSidenavModule,
        ContentModule
    ]
})
export class MainModule { }
