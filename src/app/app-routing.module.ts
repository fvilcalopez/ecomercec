import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
      path: 'auth',
      loadChildren: () => import('./security/security.module').then( m => m.SecurityModule)
    },
    {
      path: 'app',
      loadChildren: () => import('./main/main.module').then( m => m.MainModule)
    },
    { path: '**', redirectTo: 'app' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
