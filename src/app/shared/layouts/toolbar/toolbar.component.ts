import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SideNavService } from 'src/app/core/api/side-nav.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  opened = false;
  toggleActive:boolean = false;
  @Output() openCloseSideNav:EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private sidenav: SideNavService) { }

  ngOnInit(): void {
  }

  changeOpe() {
    this.toggleActive = !this.toggleActive;
    this.sidenav.toggle();
  }

}
