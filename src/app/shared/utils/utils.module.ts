import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiscountsComponent } from './discounts/discounts.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { FilterCardComponent } from './filter-card/filter-card.component';
import {SwiperModule} from "swiper/angular";
import {MatIconModule} from "@angular/material/icon";



@NgModule({
  declarations: [
    DiscountsComponent,
    ProductCardComponent,
    FilterCardComponent
  ],
  exports: [
    DiscountsComponent,
    FilterCardComponent,
    ProductCardComponent
  ],
  imports: [
    CommonModule,
    SwiperModule,
    MatIconModule
  ]
})
export class UtilsModule { }
