import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import {ProductsService} from "../../../core/api/products.service";
import SwiperCore, { Pagination, Navigation } from "swiper";
SwiperCore.use([Pagination, Navigation]);
@Component({
  selector: 'app-filter-card',
  templateUrl: './filter-card.component.html',
  styleUrls: ['./filter-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FilterCardComponent implements OnInit {
  breakpointsCategories = {
    0: {
      slidesPerView: 2
    },
    600: {
      slidesPerView: 3
    },
    1000: {
      slidesPerView: 4
    },
    1500: {
      slidesPerView: 5
    }
  };
  listCategories: any[] = []
  areas: any[] = [];
  @Output() category = new EventEmitter<string>();
  @Input() currentCategory: number = 0;
  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {
    this.productsService.getAllCategories().subscribe(
      data => {
        //this.listCategories = data;
        data.forEach((x: any) => {
          let categoryObj = {
            active: false,
            title: x
          }
          this.listCategories.push(categoryObj);
        });
        console.log(this.listCategories);
      }
    )
  }

  getCategory(idArea: any){
    this.category.emit(idArea)
    this.currentCategory = idArea;
  }


}
