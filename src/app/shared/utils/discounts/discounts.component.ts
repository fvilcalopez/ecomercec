import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import SwiperCore, { Pagination, Navigation } from "swiper";
SwiperCore.use([Pagination, Navigation]);
@Component({
  selector: 'app-discounts',
  templateUrl: './discounts.component.html',
  styleUrls: ['./discounts.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DiscountsComponent implements OnInit {
  listCoursesList: any[] = [
    {
      discount: 30,
      title: 'OFF DURRING',
      declaration: 'COVID 19',
      imgSource: 'https://image.freepik.com/foto-gratis/ilustracion-3d-smartphone-cajas-regalo-atadas-globos-mientras-flotan-cielo-compras-linea-concepto-servicio-entrega_58466-14622.jpg'
    },
    {
      discount: 20,
      title: 'OFF CHRISTMAS',
      declaration: '2021',
      imgSource: 'https://image.freepik.com/foto-gratis/ilustracion-3d-smartphone-cajas-regalo-atadas-globos-mientras-flotan-cielo-compras-linea-concepto-servicio-entrega_58466-14622.jpg'
    },
    {
      discount: 50,
      title: 'OFF NEW YEAR',
      declaration: '2021',
      imgSource: 'https://image.freepik.com/foto-gratis/ilustracion-3d-smartphone-cajas-regalo-atadas-globos-mientras-flotan-cielo-compras-linea-concepto-servicio-entrega_58466-14622.jpg'
    }
  ]
  breakpoints = {
    0: {
      slidesPerView: 1,
      slidesPerGroup: 1
    },
    600: {
      slidesPerView: 2,
      slidesPerGroup: 2
    },
    1000: {
      slidesPerView: 3,
      slidesPerGroup: 3
    },
    1200: {
      slidesPerView: 4,
      slidesPerGroup: 4
    }
  };
  constructor() { }

  ngOnInit(): void {
  }

}
