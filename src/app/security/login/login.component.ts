import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs';
import { AuthenticationService } from 'src/app/core/authentification/authentication.service';
import {LoginService} from "../../core/authentification/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin!: FormGroup;
  login = true;
  loader = false;
  constructor(private _loginService: LoginService,
              private _authenticationService: AuthenticationService,
              private route: ActivatedRoute,
              private router: Router ) {  }

  ngOnInit(): void {
    this.formLogin = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", Validators.required)
    });
  }

  onLogin(e: any) {
    this.loader = true;
    this._authenticationService.login(this.formLogin.value.username, this.formLogin.value.password).subscribe(
      data => {
        this.loader = false;
        this.sessionExitosa();
        console.log(data);
      }
    )
  }

  sessionExitosa(): void {
    let returnUrl = '';
    returnUrl =  '/app';
    const params = {};
    for (const key in this.route.snapshot.queryParams){
      if (key === 'returnUrl'){
        returnUrl = this.route.snapshot.queryParams[key];
      } else {
        // @ts-ignore
        params[key] = this.route.snapshot.queryParams[key];
      }
    }
    this.router.navigate([returnUrl], {queryParams: params} );

  }
}
