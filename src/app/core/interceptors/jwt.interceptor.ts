import {Injectable, Injector} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError, of, BehaviorSubject} from 'rxjs';
import {catchError, filter, finalize, switchMap, take} from 'rxjs/operators';
//import { AuthenticationService } from '@core/authentification/authentication.service';
import { environment } from 'src/environments/environment';
// import { environment } from 'environments/environment';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  isRefreshingToken = true;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(private inj: Injector, private router: Router,
          //    private authService: AuthenticationService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.addToken(request))
      .pipe(
        catchError((error, ca) => {
          if (error instanceof HttpErrorResponse) {
            switch ((error as HttpErrorResponse).status) {
              case 401:
               // return this.handle401Error(request, next);
              default:
                return throwError(error);
            }
          } else {
            return throwError(error);
          }
        })
      ) as any;
  }

  // @ts-ignore
  addToken(req: HttpRequest<any>): HttpRequest<any> {
   // const token = this.authService.getToken();
   // if (token && token.access_token && !req.headers.has('Authorization') && req.url.search(environment.apiHost) != -1) {
   //   return req.clone({setHeaders: {Authorization: 'Bearer ' + token.access_token, 'X-localization': 'es'}});
   // }
   // return req;

  }

  handle400Error(error: any): Observable<void> {
    console.log('400 error');
    if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
      // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
      //return this.logoutUser();
    }
    return of();
  }


  // handle401Error(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
  //   if (this.isRefreshingToken) {
  //     this.isRefreshingToken = false;
  //     // Reset here so that the following requests wait until the token
  //     // comes back from the refreshToken call.
  //     this.tokenSubject.next('');
  //     //  const http = this.inj.get(HttpClient);
  //
  //
  //     return this.authService.refreshSession().pipe(
  //       switchMap((token) => {
  //         token = this.authService.getToken();
  //         if (token) {
  //           this.tokenSubject.next(token);
  //           return next.handle(this.addToken(req));
  //         }
  //
  //         console.log('refresh failed');
  //         // If we don't get a new token, we are in trouble so logout.
  //         this.logoutUser();
  //         return of();
  //
  //       }), catchError((e: any) => {
  //         console.log('error  2' + e);
  //         // If there is an exception calling 'refreshToken', bad news so logout.
  //         this.logoutUser();
  //         return of();
  //       }),
  //       finalize(() => {
  //         console.log('token finally)');
  //         this.isRefreshingToken = true;
  //       }));
  //   } else {
  //     console.log('this.tokenSubject ' + JSON.stringify(this.tokenSubject));
  //     console.log('this.tokenSubjec2 ' + JSON.stringify(this.tokenSubject.pipe(filter(token => token != null))));
  //     return this.tokenSubject.pipe(
  //       filter(token => token != null), take(1), switchMap(token => {
  //           return next.handle(this.addToken(req));
  //         }));
  //   }
  // }

  // logoutUser(): Observable<any> {
  //   this.authService.logout(true);
  //   location.reload(true);
  //   return throwError('');
  // }
}
