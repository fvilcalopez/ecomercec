export interface IToken {
  token: string;
}
export interface IUser {
  name?: string;
  email?: string;
  password?: string;
  facebook_id?: string;
  google_id?: string;
  user_information?: any;
}

export interface IProduct {
  id:1,
  title: string,
  price: number,
  category: string,
  description: string,
  image: string,
  rating: any
}
