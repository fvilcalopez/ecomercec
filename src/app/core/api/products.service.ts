import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {IProduct, IToken} from "../Model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private url = environment.apiHost+'/products';
  constructor(private httpClient: HttpClient) {}

  getAllProducts(): Observable<IProduct[]> {
    return this.httpClient.get<IProduct[]>(this.url);
  }
  getProductsByCategory(category: string): Observable<IProduct[]> {
    return this.httpClient.get<IProduct[]>(this.url+'/category/'+category);
  }
  getAllCategories() {
    return this.httpClient.get<any>(this.url+'/categories'
    );
  }
}
