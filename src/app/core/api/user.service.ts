import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import { IUser } from '../Model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = environment.apiHost ;

  constructor(private httpClient: HttpClient) { }

  myUser(): Observable<IUser>{
    return this.httpClient.post<IUser>(this.url + '/auth/me', null).pipe();
  }

   cambiarContrasena(pass: string, matchpass: string): Observable<IUser>{
    return this.httpClient.post<IUser>(this.url + '/mi-perfil/contrasena', {password: pass, matchingPassword: matchpass }).pipe();
  }



 // userRegister(userRegister: IUserRegister): Observable<IUserRegister> {
 //   let headers = new HttpHeaders({'X-localization': 'es'})
 //   return this.httpClient.post<IUserRegister>(this.url + '/auth/register', userRegister,{headers:headers});
 // }

  eliminar(id: number): Observable<any>{
    return this.httpClient.delete(this.url + '/' + id);
  }

  obtener(id: number): Observable<IUser>{
    return this.httpClient.get<IUser>(this.url + '/' + id);
  }

  editRegister(id: number, registro: IUser): Observable<IUser> {
    return this.httpClient.put<IUser>(this.url + '/' + id, registro);
  }

  editMiPerfil( registro: IUser): Observable<IUser>{
    return this.httpClient.put<IUser>(this.url + '/auth/user', registro);
  }

}

