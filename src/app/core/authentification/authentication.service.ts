import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {LoginService} from './login.service';
import {IToken, IUser} from '../Model';
import {Rol, Role} from './roles';
import {BehaviorSubject, Observable} from 'rxjs';
import { UserService } from '../api/user.service';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  public observable: BehaviorSubject<boolean>;

  constructor(private loginService: LoginService,
              private userService: UserService) {
    this.observable = new BehaviorSubject<boolean>(this.isLogin());
  }

  login(email: string, pass: string): Observable<void> {
    return this.loginService.login(email, pass)
      .pipe(map((res: IToken) => {
        // login successful if there's a jwt token in the response
        this.successLogin(res, false);
      }));
  }

  /* iniciarSesionToken(token: string, remember: boolean): Observable<void> {
    return this.loginService.iniciarSesionToken(token)
      .pipe(map((res: IToken) => {
        // login successful if there's a jwt token in the response
        this.inicioExitoso(res, remember);
      }));
  }*/

  refreshSession(): Observable<any> {
    return this.loginService.refresh(this.getToken().token)
      .pipe(map((res: IToken) => {
        // login successful if there's a jwt token in the response
        if (res && res.token) {
          // decodedJwtData.authorities;
          // store username and jwt token in local storage to keep user logged in between page refreshes
          const recuerdame: boolean = this.isRememberme();
          if (recuerdame) {
            localStorage.setItem('token_project', JSON.stringify(res));
          } else {
            sessionStorage.setItem('token_project', JSON.stringify(res));
          }
          this.observable.next(true);
        }
      }));
  }

    loadUser(): Observable<any> {
    return this.userService.myUser().pipe(map((user: IUser) => {
      localStorage.setItem('current_user_project', JSON.stringify(user));
    }));
  }

  logout(noBorrar: boolean): void {
    // remove user from local storage to log user out
    localStorage.removeItem('rememberme');
    localStorage.removeItem('token_project');
    sessionStorage.removeItem('token_project');
    localStorage.removeItem('current_user_project');
    sessionStorage.removeItem('current_user_project');
    if (!noBorrar) {
      this.observable.next(false);
    }
  }

  getToken(): IToken {
    const recuerdame: boolean = this.isRememberme();
    let token: string | null;
    if (recuerdame) {
      token = localStorage.getItem('token_project');
    } else {
      token = sessionStorage.getItem('token_project');
    }
    return JSON.parse(token as string) as IToken;
  }

  getUser(): IUser {
    const recuerdame: boolean = this.isRememberme();
    let user: string | null;
    if (recuerdame) {
      user = localStorage.getItem('current_user_project');
    } else {
      user = sessionStorage.getItem('current_user_project');
    }
    return JSON.parse(user as string) as IUser;
  }

  updateUser(user: IUser): void {
    localStorage.removeItem('current_user_project');
    const recuerdame: boolean = this.isRememberme();
    if (recuerdame) {
      localStorage.setItem('current_user_project', JSON.stringify(user));
    } else {
      sessionStorage.setItem('current_user_project', JSON.stringify(user));
    }
  }

  getRoles(): Role[] {
    const roles: Role[] = [];
    const user = JSON.parse(<string>localStorage.getItem('current_user_project')) ;
    user.rols.forEach((x: any )=>{
      roles.push(Rol.fromInt(x.id));
    });
    return roles;
  }


  hasRolPermision(roles: Role[]): boolean {
    const rolesSaved = this.getRoles();
    if (roles.length == 0) {
      return true;
    }
    for (var i = 0; i < roles.length; i++) {
      for (var j = 0; j < rolesSaved.length; j++) {
        if (roles[i] == rolesSaved[j]) {
          return true;
        }
      }
    }
    return false;
  }

  isLogin(): boolean {
    return !!this.getToken();
  }

  isRememberme(): boolean {
    return localStorage.getItem('rememberme') === 'true';
  }

  private successLogin(res: IToken, recuerdame: boolean): void {
    if (res && (res.token)) {
      localStorage.setItem('rememberme', recuerdame + '');
      if (recuerdame) {
        localStorage.setItem('token_project', JSON.stringify(res));
      } else {
        sessionStorage.setItem('token_project', JSON.stringify(res));
      }

      this.observable.next(true);
    }
  }
}
