import { Injectable } from '@angular/core';
import { IToken } from '../Model';
import {HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url = environment.apiHost;
  registerStatus:any

  constructor(private httpClient: HttpClient) { }

  private getAutorization(): HttpHeaders{
    const headers = new HttpHeaders();
    return headers;
  }

  login(username: string, password: string): Observable<IToken> {
    return this.httpClient.post<IToken>(this.url + '/auth/login' , {
      username: username,
      password: password
    } );
  }


  refresh(refreshToken: string): Observable<IToken> {
    const headers = this.getAutorization();
    return this.httpClient.patch<IToken>(this.url + '/auth/refresh' ,
      {grant_type: 'refresh_token',
        token: refreshToken} , {
        headers
      });
  }


}
