export enum Role {
  Gratuito = 1,
  Pagado = 2,
  Ponente = 3,
  Administrador = 4
}

// tslint:disable-next-line:no-namespace
export namespace Rol {
  export function fromInt(s: number): Role{
    let r: Role| null = null;
    for (const item in Role) {
      // @ts-ignore
      if (Role[item.toString()] === s) {
        // @ts-ignore
        r = Role[item.toString()] as Role;
        break;
      }
    }
    let r1: Role;
    r1 = r as Role;
    return r1;
  }


  export function getName(role: Role): string{
    let name = '';
    switch (role) {
      case Role.Administrador:
        name = 'Administrador';
        break;
      case Role.Gratuito:
        name = 'Gratuito';
        break;
      case Role.Pagado:
        name = 'Pagado';
        break;
      case Role.Ponente:
        name = 'Ponente';
        break;
      default:
        break;
    }
    return name;
  }
}

